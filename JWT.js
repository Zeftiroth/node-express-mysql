const { sign, verify } = require('jsonwebtoken');

const createToken = (user) => {
	const accessToken = sign({ name: user.name, id: user.id }, process.env.JWT);
	return accessToken;
};

const validateToken = (req, res, next) => {
	console.log('object');
	const accessToken = req.cookies['access-token'];
	if (!accessToken) {
		return res.status(400).json({ msg: 'Access token not found' });
	}
	try {
		const validToken = verify(accessToken, process.env.JWT);
		if (!validToken) {
			return res.status(400).json({ msg: 'Access token not valid' });
		}
		if (validToken) {
			req.authenticated = true;
			console.log('object');
			return next();
		}
	} catch (error) {
		return res.status(400).json({ msg: error });
	}
};

module.exports = { createToken, validateToken };
