require('dotenv').config();
const mysql = require('mysql');

const pool = mysql.createPool({
	connectionLimit: 10,
	user: process.env.DB_USER,
	password: process.env.DB_PW,
	database: process.env.DB_NAME,
	host: process.env.DB_HOST,
	port: 3306,
});

let testdb = {};

testdb.all = () => {
	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM user', (err, result) => {
			if (err) {
				return reject(err);
			}
			return resolve(result);
		});
	});
};

testdb.selectOneById = (id) => {
	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM user WHERE id = ?', [id], (err, result) => {
			if (err) {
				return reject(err);
			}
			return resolve(result[0]);
		});
	});
};

testdb.selectOne = (obj) => {
	return new Promise((resolve, reject) => {
		pool.query(
			`SELECT * FROM user WHERE ${Object.keys(obj)} = ?`,
			[Object.values(obj)],
			(err, result) => {
				if (err) {
					return reject(err);
				}
				return resolve(result[0]);
			}
		);
	});
};

testdb.updateOneById = (id, column) => {
	return new Promise((resolve, reject) => {
		let sqlkeyValue = [];
		let intId = parseInt(id);

		for (let i in column) {
			let keyValueString = `${i} = "${column[i]}"`;
			sqlkeyValue.push(keyValueString);
			console.log(sqlkeyValue);
		}

		let sqlCol = sqlkeyValue.join(', ');
		console.log(sqlCol);

		pool.query(
			`UPDATE user SET ${sqlCol} WHERE id = ?`,
			[intId],
			(err, result) => {
				if (err) {
					return reject(err);
				}
				return resolve(result[0]);
			}
		);
	});
};

testdb.createOne = (column) => {
	return new Promise((resolve, reject) => {
		let key = [];
		let val = [];

		for (let i in column) {
			let keyString = `${i}`;
			key.push(keyString);
			let valString = `${column[i]}`;
			val.push(valString);
			console.log(key);
			console.log(val);
		}
		let arrLen = key.length;
		let questionMark = '?,';
		let questionMarkString = questionMark.repeat(arrLen).slice(0, -1);
		console.log(questionMarkString);

		let sqlCol = key.join(', ');
		console.log(sqlCol);

		pool.query(
			`INSERT INTO user (${sqlCol}) VALUES (${questionMarkString})`,
			val,
			(err, result) => {
				if (err) {
					return reject(err);
				}
				return resolve(result[0]);
			}
		);
	});
};

module.exports = testdb;
