const express = require('express');
const router = express.Router();
const db = require('../db');
const bcrypt = require('bcryptjs');

const { createToken, validateToken } = require('../../JWT');

router.get('/test/auth', async (req, res) => {
	try {
		console.log('first');
		res.status(200).json({ auth: true, msg: 'token is valid' });
	} catch (error) {
		res.status(500).json({ error });
	}
});

router.get('/test/logout', async (req, res) => {
	try {
		res.clearCookie('access-token');
		res.status(200).json({ auth: true, msg: 'clear cookies successfully' });
	} catch (error) {
		res.status(500).json(error);
	}
});

router.get('/', (req, res) => {
	res.json({ message: 'Welcome to Troopers Backend.' });
});

router.get('/test', async (req, res) => {
	try {
		let results = await db.all();
		results.forEach((obj) => {
			delete obj['pw'];
		});
		console.log(results);
		res.json(results);
	} catch (e) {
		res.sendStatus(500);
		console.log(e);
	}
});

router.get('/test/:id', async (req, res) => {
	try {
		let results = await db.selectOneById(req.params.id);
		res.json(results);
	} catch (e) {
		res.sendStatus(500);
		console.log(e);
	}
});

router.post('/test/login', async (req, res) => {
	let loginInfo = {
		email: req.body.email,
		password: req.body.pw,
	};
	try {
		let results = await db.selectOne({ email: loginInfo.email });
		console.log(results);
		console.log(results.pw);

		if (!results) {
			res.json({ msg: 'no results' });
		}
		let comparePw = await bcrypt.compare(loginInfo.password, results.pw);
		console.log(comparePw);
		if (comparePw) {
			const accessToken = createToken(results);
			res.cookie('access-token', accessToken, {
				maxAge: 30 * 24 * 60 * 60 * 1000,
			});
			return res.json(results);
		} else {
			res.sendStatus(500);
		}
	} catch (e) {
		res.sendStatus(500);
		console.log(e);
	}
});

router.put('/test/:id', async (req, res) => {
	let column = req.body;
	try {
		let results = await db.selectOneById(req.params.id);
		res.json(results);
		if (column.name === '') {
			column.name = results.name;
		}
		if (column.dob === '') {
			column.dob = results.dob;
		}
		if (column.phone_number === '') {
			column.phone_number = results.phone_number;
		}
		if (column.address === '') {
			column.address = results.address;
		}
	} catch (e) {
		res.sendStatus(500);
		console.log(e);
	}
	try {
		let results = await db.updateOneById(req.params.id, column);
		res.json(results);
	} catch (e) {
		res.sendStatus(500);
		console.log(e);
	}
});

router.post('/test/register', async (req, res) => {
	let hashedPw = await bcrypt.hash(req.body.pw, 8);
	let column = {
		name: req.body.name,
		email: req.body.email,
		pw: hashedPw,
	};
	console.log(hashedPw);

	try {
		let results = await db.createOne(column);
		res.json(results);
	} catch (e) {
		res.status(500).send(e);
		console.log(e);
	}
});

module.exports = router;
