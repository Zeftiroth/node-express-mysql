require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
const apiRouter = require('./app/routes/test.routes');
const cookieParser = require('cookie-parser');

const corsOptions = {
	origin: '*',
	methods: ['GET', 'POST', 'PUT', 'DELETE'],
	allowedHeaders: [
		'Access-Control-Allow-Headers',
		'Origin',
		'X-Requested',
		'Content-Type',
		'Accept',
		'Authorization',
	],
};
//Cors Configuration - Start
// app.use((req, res, next) => {
// 	res.header('Access-Control-Allow-Origin', '*');
// 	res.header(
// 		'Access-Control-Allow-Headers',
// 		'Origin, X-Requested, Content-Type, Accept Authorization'
// 	);
// 	if (req.method === 'OPTIONS') {
// 		res.header('Access-Control-Allow-Methods', 'POST, PUT, PATCH, GET, DELETE');
// 		return res.status(200).json({});
// 	}
// 	next();
// });
//Cors Configuration - End
// add logger
// app.use(logger);

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// add cookieParser
app.use(cookieParser());
// simple route
app.use('/', apiRouter);

function logger(req, res, next) {
	console.log({ req: req, res: res });
	next();
}

// set port, listen for requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});
